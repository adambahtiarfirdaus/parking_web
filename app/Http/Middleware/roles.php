<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class roles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        if (!Auth::check()) // I included this check because you have it, but it really should be part of your 'auth' middleware, most likely added as part of a route group.
        return redirect('login');

        $user = Auth::user();

        if($user->roles == "super_admin")
            return $next($request);
        foreach($roles as $role) {
            if($user->roles == $role)
                return $next($request);
        }
        return redirect('login');
    }
}
