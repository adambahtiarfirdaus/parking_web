<?php

namespace App\Http\Controllers;

use App\Parking;
use Illuminate\Http\Request;
use Auth;
class ParkingController extends Controller
{

    public function index()
    {

        if (Auth::user()->roles == 'user') {
            $data = Parking::where('id_user',Auth::id())->paginate(10);
            return view('pages.parking.index_for_user',[
                'data' => $data
            ]);
        }elseif(Auth::user()->roles == 'super_admin'){
            $data = Parking::where('id_user',Auth::id())->paginate(10);
            return view('pages.parking.index_for_super',[
                'data' => $data
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Parking  $parking
     * @return \Illuminate\Http\Response
     */
    public function show(Parking $parking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Parking  $parking
     * @return \Illuminate\Http\Response
     */
    public function edit(Parking $parking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Parking  $parking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Parking $parking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Parking  $parking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Parking $parking)
    {
        //
    }

    public function report()
    {
        return view('pages.parking.report_for_super');
    }
}
