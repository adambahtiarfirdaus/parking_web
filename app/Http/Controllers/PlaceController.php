<?php

namespace App\Http\Controllers;

use App\Place;
use Illuminate\Http\Request;

class PlaceController extends Controller
{

    public function index()
    {
        $data = Place::latest()->get();
        return view('pages.place.index',[
            'place' => $data
        ]);
    }

    public function create()
    {
        return view('pages.place.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:200',
            'address' => 'required|string|300',
            'long' => 'required',
            'lat' => 'required'
        ]);

        try {
            Place::create([
                'name'=> $request->name,
                'address' => $request->address,
                'long' => $request->long,
                'lat' => $request->lat,
            ]);
            return redirect()->back()->with('message','success');

        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['msg'=>'Opps Something Wrong']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function show(Place $place)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function edit(Place $place)
    {
        return view('pages.place.edit',['place' => $place]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Place $place)
    {
        $this->validate($request,[
            'name' => 'required|string|max:200',
            'address' => 'required|string|300',
            'long' => 'required',
            'lat' => 'required'
        ]);

        try {
            $place->update([
                'name'=> $request->name,
                'address' => $request->address,
                'long' => $request->long,
                'lat' => $request->lat,
            ]);
            return redirect()->back()->with('message','success');

        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['msg'=>'Opps Something Wrong']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function destroy(Place $place)
    {

        try {
            $place->delete();

        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['msg'=>'Opps Something Wrong']);
        }
    }
}
