<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Illuminate\Support\Str;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        $this->username = $this->findUsername();
    }
    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findUsername()
    {
        $login = request()->input('login');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        request()->merge([$fieldType => $login]);

        return $fieldType;
    }

    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        return $this->username;
    }

    public function redirectToProvider($driver)
    {
        return Socialite::driver($driver)->redirect();
    }

    // public function handleProviderCallback($driver)
    // {
    //     try {
    //         $user = Socialite::driver($driver)->user();

    //         $create = User::firstOrCreate([
    //             'email' => $user->getEmail()
    //         ], [
    //             'socialite_name' => $driver,
    //             'socialite_id' => $user->getId(),
    //             'username' => Str::random(32),
    //             'roles' => 'user',
    //             'name' => $user->getName(),
    //             'photo' => $user->getAvatar(),
    //             'email_verified_at' => now()
    //         ]);
    //         auth()->login($create, true);
    //         return redirect($this->redirectPath());
    //     } catch (\Exception $e) {
    //         return redirect()->route('login');
    //     }
    // }
    public function handleProviderCallback($driver)
    {
        try {
            $user = Socialite::driver($driver)->user();
            $authUser = $this->findOrCreateUser($user, $driver);
            $auth = auth()->login($authUser, true);
            return redirect('/home');
        } catch (\Exception $e) {
            return redirect()->route('login');
        }
    }
    public function findOrCreateUser($user, $driver)
    {
        $authUser = User::where('socialite_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        else{
            $data = User::firstOrCreate([
                    'email' => $user->getEmail(),
                    'socialite_name' => $driver,
                    'socialite_id' => $user->id,
                    'name' => $user->getName(),
                    'photo' => $user->getAvatar(),
                    'username' => Str::random(32),
                    'roles' => 'user',
                ]);
            return $data;
        }
    }
}
