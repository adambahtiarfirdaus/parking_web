<?php

namespace App\Http\Controllers;

use App\UnitTypeSetting;
use Illuminate\Http\Request;

class UnitTypeSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UnitTypeSetting  $unitTypeSetting
     * @return \Illuminate\Http\Response
     */
    public function show(UnitTypeSetting $unitTypeSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UnitTypeSetting  $unitTypeSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(UnitTypeSetting $unitTypeSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UnitTypeSetting  $unitTypeSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UnitTypeSetting $unitTypeSetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UnitTypeSetting  $unitTypeSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(UnitTypeSetting $unitTypeSetting)
    {
        //
    }
}
