<?php

namespace App\Http\Controllers;

use App\PlaceManager;
use Illuminate\Http\Request;

class PlaceManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PlaceManager  $placeManager
     * @return \Illuminate\Http\Response
     */
    public function show(PlaceManager $placeManager)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PlaceManager  $placeManager
     * @return \Illuminate\Http\Response
     */
    public function edit(PlaceManager $placeManager)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PlaceManager  $placeManager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlaceManager $placeManager)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PlaceManager  $placeManager
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlaceManager $placeManager)
    {
        //
    }
}
