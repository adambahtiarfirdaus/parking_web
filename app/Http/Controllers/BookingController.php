<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Place;
use App\UnitTypeSetting;
use App\Parking;
use App\Payment;
use Crypt;
use Auth;
use Carbon\Carbon;
class BookingController extends Controller
{
    public function __contruct()
    {
        $this->middleware('auth,roles:user');
    }

    public function search(Request $request)
    {
        $s = $request->s;
        $result = Place::where('name','like',"%".$s."%")
                ->orWhere('address','like','%'.$s.'%')
                ->paginate();
        return view('pages.booking.result',[
            'result' => $result
        ]);
    }
    public function order($id)
    {
        $id = Crypt::decrypt($id);
        $place = Place::find($id);

        return view('pages.booking.order',[
            'place' => $place
        ]);

    }
    public function getTypePrice(Request $request)
    {
        $id = Crypt::decrypt($request->id);
        $data = UnitTypeSetting::find($id);
        // $price_value = Crypt::encrypt($data->booking_price);
        $price_text = $data->booking_price;

        return response()->json([
            // 'price_value'=>$price_value,
            'price_text' => $price_text
            ]);
    }

    public function store(Request $request)
    {

        $this->validate($request,[
            'id_place' => 'required',
            'vehicle_type' => 'required',
            'license_plate' => 'required',
            'booking_time' => 'required'
        ]);
        $placeTypeVehicle = UnitTypeSetting::find(Crypt::decrypt($request->vehicle_type));
        $total = $placeTypeVehicle->booking_price * $request->booking_time;

        $data = Parking::create([
            'id_author' => Auth::id(),
            'id_place' => Crypt::decrypt($request->id_place),
            'id_user' => Auth::id(),
            'license_plate' => $request->license_plate,
            'type' => 'booking',
            'booking_exp' => Carbon::now()->addHours($request->booking_time),
            'status' => 'reserved',
        ]);

        $payment = Payment::create([
            'id_parking' => $data->id,
            'payment' => 'gopay',
            'type' => 'booking',
            'status' => 'done',
            'mount' =>$total
        ]);

        return redirect()->route('booking.success');
    }

    public function success()
    {
        return view('pages.booking.success');
    }
}
