<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd(Auth::user()->roles);
        if(Auth::user()->roles == 'super_admin'){
            return redirect()->route('super_admin.index');
        }else if (Auth::user()->roles == 'admin') {
            return redirect()->route('admin.index');
        }
        else if (Auth::user()->roles == 'operator') {
            return redirect()->route('operator.index');
        }
        return view('pages.booking.index');
    }
}
