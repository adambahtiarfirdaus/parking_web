<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parking extends Model
{
    protected $guarded = [];
    public function user()
    {
        return $this->hasOne('App\User', 'id_user','id');
    }
    public function place()
    {
        return $this->hasOne('App\Place', 'id', 'id_place');
    }
    public function payment()
    {
        return $this->hasMany('App\Payment','id_parking', 'id');
    }
}
