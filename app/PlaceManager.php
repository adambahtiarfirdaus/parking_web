<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaceManager extends Model
{
    public function user()
    {
        return $this->hasOne('App\User'.'id','id_user');
    }

    public function place()
    {
        return $this->hasOne('App\Place'.'id','id_place');
    }
}
