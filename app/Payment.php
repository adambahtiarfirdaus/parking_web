<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $guarded = [];
    public function parking()
    {
        return $this->hasOne('App\Parking', 'id', 'id_parking');
    }
}
