<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Place extends Model
{

    public function managers()
    {
        return $this->hasMany('App\PlaceManager', 'id_user', 'id');
    }

    public function setting()
    {
        return $this->hasOne('App\PlaceSetting','id_place','id');
    }

    public function units()
    {
        return $this->hasMany('App\UnitTypeSetting', 'id_place', 'id');
    }

}
