<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return redirect('login');
});

Auth::routes(['register' => false]);
Route::get('redirect/{driver}', 'Auth\LoginController@redirectToProvider')->name('login.provider');
Route::get('{driver}/callback', 'Auth\LoginController@handleProviderCallback')->name('login.callback');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'super_admin','middleware' => ['auth','roles:super_admin']], function () {
    Route::get('/', 'SuperAdminController@index')->name('super_admin.index');
    Route::resource('user', 'UserController');
});

Route::group(['prefix' => 'admin','middleware' => ['auth','roles:admin']], function () {
    Route::get('/', 'AdminController@index')->name('admin.index');
});

Route::group(['prefix' => 'operator','middleware' => ['auth','roles:operator']], function () {
    Route::get('/', 'OperatorController@index')->name('operator.index');
});

Route::group(['middleware' => ['auth','roles:user']], function () {
    Route::get('booking/search', 'BookingController@search')->name('booking.search');
    Route::get('booking/order/{id}', 'BookingController@order')->name('booking.order');
    Route::post('booking/get_type_price', 'BookingController@getTypePrice')->name('booking.get_type_price');
    Route::post('booking/store', 'BookingController@store')->name('booking.store');
    Route::get('booking/success', 'BookingController@success')->name('booking.success');
    Route::get('parking/report', 'ParkingController@report')->name('parking.report');
    Route::resource('parking', 'ParkingController');
});



