<div class="topnav">
    <div class="container-fluid">
        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

            <div class="collapse navbar-collapse" id="topnav-menu-content">
                <ul class="navbar-nav">
                    {{-- item untuk super_admin --}}
                    @if (Auth::user()->roles == 'super_admin')
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/home') }}">
                            <i class="mdi mdi-storefront mr-2"></i>Dashboard
                        </a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="lahan_parkir" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-cloud-print-outline mr-2"></i>Lahan Parkir  <div class="arrow-down"></div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="lahan_parkir">
                            <a href="{{ route('parking.index') }}" class="dropdown-item">Data</a>
                            <a href="{{ route('parking.report') }}" class="dropdown-item">Report</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="users" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-cloud-print-outline mr-2"></i>Users<div class="arrow-down"></div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="users">
                            <a href="{{ route('user.index') }}" class="dropdown-item">Data</a>
                        </div>
                    </li>

                    {{-- item untuk admin --}}
                    @elseif(Auth::user()->roles == 'admin')
                    <li class="nav-item">
                        <a class="nav-link" href="index.html">
                            <i class="mdi mdi-storefront mr-2"></i>Dashboard
                        </a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="lahan_parkir" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-cloud-print-outline mr-2"></i>Lahan Parkir  <div class="arrow-down"></div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="lahan_parkir">
                            <a href="calendar.html" class="dropdown-item">Data</a>
                            <a href="calendar.html" class="dropdown-item">Report</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="users" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-cloud-print-outline mr-2"></i>Users<div class="arrow-down"></div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="users">
                            <a href="calendar.html" class="dropdown-item">Data</a>
                            <a href="calendar.html" class="dropdown-item">Report</a>
                        </div>
                    </li>
                    {{-- item untuk operator --}}
                    @elseif(Auth::user()->roles == 'operator')
                    <li class="nav-item">
                        <a class="nav-link" href="index.html">
                            <i class="mdi mdi-storefront mr-2"></i>Dashboard
                        </a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="lahan_parkir" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-cloud-print-outline mr-2"></i>Lahan Parkir  <div class="arrow-down"></div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="lahan_parkir">
                            <a href="calendar.html" class="dropdown-item">Data</a>
                            <a href="calendar.html" class="dropdown-item">Report</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="users" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-cloud-print-outline mr-2"></i>Users<div class="arrow-down"></div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="users">
                            <a href="calendar.html" class="dropdown-item">Data</a>
                            <a href="calendar.html" class="dropdown-item">Report</a>
                        </div>
                    </li>
                    {{-- item untuk user --}}
                    @elseif(Auth::user()->roles == 'user')
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/home') }}">
                            <i class="mdi mdi-book mr-2"></i>Booking
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('parking.index') }}">
                            <i class="mdi mdi-ticket-confirmation mr-2"></i>Pesanan
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
        </nav>
    </div>
</div>
