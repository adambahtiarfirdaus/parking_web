@extends('layouts.app_new')

@section('title','Result')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"></h4>
                    <hr>
                    <div id="search" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header" role="tab" id="section1HeaderId">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#search" href="#section1ContentId" aria-expanded="true" aria-controls="section1ContentId">
                              Cari
                            </a>
                                </h5>
                            </div>
                            <div id="section1ContentId" class="collapse in" role="tabpanel" aria-labelledby="section1HeaderId">
                                <div class="card-body">
                                    <form action="{{ route('booking.search') }}" method="get">
                                        <div class="form-group">
                                          <label for="">Nama Lokasi</label>
                                          <input type="text" name="s" id="" class="form-control" placeholder="masukan lokasi" aria-describedby="helpId">
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block btn-lg">Cari</button>
                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <br>
            @if (count($result) < 1)
            <div class="alert alert-danger" role="alert">
                <strong>Parkiran Tidak Ditemukan</strong>
            </div>
            @endif
            @foreach ($result as $item)
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $item->name }}</h4>
                    <p class="card-text">{{ $item->address }}</p>
                    <hr>
                    <div id="price_list" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header" role="tab" id="section1HeaderId">
                                <p class="mb-0">
                                    <a data-toggle="collapse" data-parent="#price_list" href="#sectiondetail{{ $loop->index }}" aria-expanded="true" aria-controls="sectiondetail{{ $loop->index }}">
                              Detail Harga
                            </a>
                                </p>
                            </div>
                            <div id="sectiondetail{{ $loop->index }}" class="collapse in" role="tabpanel" aria-labelledby="section1HeaderId">
                                <div class="card-body">
                                    <table class="table">
                                        <tr>
                                            <th>Tipe</th>
                                            <th>Per Jam</th>
                                            <th>Per Booking</th>
                                        </tr>
                                        @foreach ($item->units as $unit)
                                        <tr>
                                            <td>{{ $unit->name }}</td>
                                            <td>{{ $unit->price }}</td>
                                            <td>{{ $unit->booking_price }}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a name="" id="" class="btn btn-success btn-lg btn-block" href="{{ route('booking.order',\Crypt::encrypt($item->id)) }}" role="button">Pesan</a>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
@endsection
