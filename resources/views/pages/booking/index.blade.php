@extends('layouts.app_new')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Cari Lahan Parkir</h4>
                    <hr>
                    <form action="{{ route('booking.search') }}" method="get">
                    <div class="form-group">
                      <label for="">Nama Lokasi</label>
                      <input type="text" name="s" id="" class="form-control" placeholder="masukan lokasi" aria-describedby="helpId">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-lg">Cari</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
