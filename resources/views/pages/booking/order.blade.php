@extends('layouts.app_new')


@section('title','Order')
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Pesan </h5>
                    <hr>
                    <form action="{{ route('booking.store') }}" method="post">
                    @csrf
                    <h4>{{ $place->name }}</h4>
                    <p>{{ $place->address }}</p>
                    <hr>
                    <input type="hidden" name="id_place" value="{{ \Crypt::encrypt($place->id) }}">
                    <div class="form-group">
                      <label for="nopol">Nopol</label>
                      <input type="text" name="license_plate" id="nopol" class="form-control" placeholder="masukan nomor polisi kendaraan anda" aria-describedby="">
                    </div>
                    <div class="form-group">
                      <label for="">Tipe Kendaraan</label>
                      <select class="form-control" name="vehicle_type" id="type">
                          <option selected disabled value="">---</option>
                          @foreach ($place->units as $type)
                            <option value="{{ \Crypt::encrypt($type->id) }}" data-price="{{ $type->booking_price }}">{{ $type->name }}</option>
                          @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Waktu Booking</label>
                      <select class="form-control" name="booking_time" id="booking_time">

                        @foreach (explode(',',$place->setting->booking_time) as $item)
                        <option value="{{ $item }}">{{ $item ." Jam"}}</option>
                        @endforeach
                      </select>
                    </div>
                    <br>
                    <div class="text-center">
                    <h1 class="text-success" id="price_text"></h1>
                    </div>
                    <br>
                </div>
                <div class="card-footer">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success btn-block btn-lg" id="submit_button" disabled data-toggle="modal" data-target="#modelId">
                      Metode Pembayaran
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Pembayaran</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                </div>
                                <div class="modal-body">
                                    <h1 class="text-success text-center">PANEL PEMBAYARAN</h1>
                                    <H5 class=" text-center">BELUM TERSEDIA</H5>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-success btn-block btn-lg" >Pesan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        let con_price = 0;
        $(document).ready(function(){
            $('#type').change(function(){
                var id = $(this).val();
                let _token   = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url : "{{ route('booking.get_type_price') }}",
                    type: "POST",
                    data:{
                        id:id,
                        _token: _token
                    },success : function (response) {
                        let booking_time = $('#booking_time').val();
                        con_price = parseInt(response.price_text) * booking_time;
                        $('#submit_button').prop( "disabled", false );
                        $('#price_text').html("Rp "+ con_price);
                    }
                });
            });

            $('#booking_time').change(function () {
                let price_type = $('#type').find(':selected').data('price');
                let con_price = (parseInt(price_type) * parseInt($(this).val()));
                $('#price_text').html("Rp "+ con_price);
            });
        });
    </script>
@endsection
