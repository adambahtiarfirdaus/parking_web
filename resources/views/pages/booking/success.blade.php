@extends('layouts.app_new')

@section('title','Payment Success')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header"></div>
                <div class="card-body">
                    <center><img width="100" height="100" src="{{ asset('check.png') }}" alt=""></center>
                    <br>
                    <h1 class="text-success">Payment Success</h1>
                    <br>
                    <a name="" id="" class="btn btn-primary btn-block" href="{{ route('booking.search') }}" role="button">Oke</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
