@extends('layouts.app_new')

@section('title','Pesananmu')
@section('content')
<div class="container">
    <div class="row ">
        @foreach ($data as $item)
            <div class="col-md-4 offset-md-1">
                <div class="card">
                    <div class="card-body">
                        <br>
                        <h4 class="">{{ $item->place->name }}</h4>
                        <p>{{ $item->place->address }}</p>
                        <hr>
                        <table class="table table-responsive ">
                            <tr class="bg-dark text-white">
                                <th>Tipe</th>
                                <th>Jumlah</th>
                                <th>Status</th>
                            </tr>
                            @foreach ($item->payment as $payment_list)
                                <tr>
                                    <td> {{ Str::upper($payment_list->type)  }} </td>
                                    <td>{{ $payment_list->mount }}</td>
                                    <td>
                                        @php
                                        if($payment_list->status  == 'done'){
                                            echo '<span class="badge badge-success">Berhasil</span>';
                                        }elseif ($payment_list->status  == 'pending') {
                                            echo '<span class="badge badge-warning">Pending</span>';
                                        }else{
                                            echo '<span class="badge badge-danger"></span>';
                                        }
                                        @endphp
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <br>
                        @if ($payment_list->status == "done")
                        <!-- Button trigger modal -->
                        <div class="container">
                            <button type="button" class="btn btn-primary btn-block " data-toggle="modal" data-target="#modaldetail{{ $loop->index }}">
                                QRCODE
                              </button>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="modaldetail{{ $loop->index }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Detail</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                    </div>
                                    <div class="modal-body">
                                        <p for="" class="text-center">Tiket Kadaluarsa</p>
                                        <h4 class="text-center text-success">{{ $item->booking_exp }}</h4>
                                        <p  class="text-center">Barcode</p>
                                        <center>
                                            {{-- {!! DNS1D::getBarcodeHTML(\Crypt::encrypt($item->id), 'QRCODE')!!} --}}
                                            {!! DNS2D::getBarcodeHTML(\Crypt::encrypt($item->id), 'QRCODE',5,5); !!}
                                            <small>*Berikan barcode ke petugas untuk masuk</small>
                                        </center>
                                        <br>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        @endforeach
        {{ $data->links() }}
    </div>
</div>
@endsection


