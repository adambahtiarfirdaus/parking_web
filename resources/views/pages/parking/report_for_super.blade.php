@extends('layouts.app_new')

@section('title','Data Lahan Parkir')
@section('style')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('content')
<div class="card card-body">
    <form action="">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                  <label for="code">kode Parkir</label>
                  <input type="text" name="code" id="code" class="form-control" placeholder="" aria-describedby="">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="code">Nopol</label>
                    <input type="text" name="code" id="code" class="form-control" placeholder="" aria-describedby="">
                </div>
            </div>
            <div class="col-md-3">
                    <div class="form-group">
                        <label for="from_date">Tanggal Awal</label>
                        <input type="date" name="from_date" id="from_date" class="form-control" placeholder="" aria-describedby="">
                    </div>
                    <div class="form-group">
                        <label for="to_date">Tanggal Akhir</label>
                        <input type="date" name="to_date" id="to_date" class="form-control" placeholder="" aria-describedby="">
                    </div>
            </div>
            <div class="col-md-3">
                <br>
                <button type="submit" class="btn btn-primary btn-block">Cari</button>
            </div>
        </div>
    </form>
</div>
<div class="container">
    <div class="card">
        <div class="card-body">
            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead>
                <tr>
                    <th>Kode Parkir</th>
                    <th>Nopol</th>
                    <th>Tipe</th>
                    <th>Jam masuk</th>
                    <th>Jam keluar</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
                </thead>

                <tbody>
                    <tr>
                        <th>AGTYSD123AS</th>
                        <th>B 123 AR</th>
                        <th>Mobil</th>
                        <th>12:08:22</th>
                        <th>14:08:22</th>
                        <th class="text-center">
                            <span class="badge badge-success">Selesai</span>
                        </th>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="action" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                            Aksi
                                        </button>
                                <div class="dropdown-menu" aria-labelledby="action">
                                    <a class="dropdown-item" href="#">Detail</a>
                                    <a class="dropdown-item" href="#">Setting</a>
                                    <a class="dropdown-item" href="#">Edit</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item text-danger" href="#">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
<!-- Required datatable js -->
<script src="{{ asset('assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/libs/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('assets/libs/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/libs/pdfmake/build/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection


