<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitTypeSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_type_settings', function (Blueprint $table) {
            $table->id();
            $table->integer('id_author');
            $table->integer('id_place');
            $table->string('name');
            $table->string('capacity');
            $table->integer('price');
            $table->integer('overnight_price');
            $table->integer('booking_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_type_settings');
    }
}
