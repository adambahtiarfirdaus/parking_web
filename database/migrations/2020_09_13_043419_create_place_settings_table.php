<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaceSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('place_settings', function (Blueprint $table) {
            $table->id();
            $table->integer('id_author');
            $table->integer('id_place');
            $table->enum('type',['directly_pay','pay_per_hour']);
            // $table->integer('price')->nullable();
            $table->enum('overnight', ['yes','no']);
            // $table->integer('overnight_price')->nullable();
            $table->enum('bookingable', ['yes','no']);
            // $table->integer('booking_price')->nullable();
            $table->string('booking_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('place_settings');
    }
}
