<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParkingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parkings', function (Blueprint $table) {
            $table->id();
            $table->integer('id_author');
            $table->integer('id_place');
            $table->integer('id_user')->nullable();
            $table->integer('license_plate');
            $table->dateTime('time_on_entry')->nullable();
            $table->dateTime('time_on_exit')->nullable();
            $table->enum('type',['on_site','booking']);
            $table->dateTime('booking_exp')->nullable();
            $table->enum('status',['reserved','ongoing','done','cancel','failed']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parkings');
    }
}
