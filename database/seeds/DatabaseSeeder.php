<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'super',
            'username' => 'super',
            'email' => 'super@mail.com',
            'phone' => '0899778732882',
            'password'=> Hash::make('super123'),
            'email_verified_at' => now(),
            'roles'=> 'super_admin'
        ]);

        User::create([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@mail.com',
            'phone' => '0856153155404',
            'password'=> Hash::make('admin123'),
            'email_verified_at' => now(),
            'roles'=> 'admin'
        ]);

        User::create([
            'name' => 'operator',
            'username' => 'operator',
            'email' => 'operator@mail.com',
            'phone' => '08997787762',
            'password'=> Hash::make('operator'),
            'email_verified_at' => now(),
            'roles'=> 'user'
        ]);
    }
}
